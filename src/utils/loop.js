'use strict';

class Loop {
    static loop(start, stop, callback, done) {
        let task = null;
        let current = start;
        const iterator = internalLoop;
        function internalLoop() {
            if (current >= stop) {
                clearInterval(task);
                if (done) {
                    done();
                }
            } else {
                callback(current++);
            }
        }
        task = setInterval(iterator, 0);
    }
}

module.exports = Loop;
