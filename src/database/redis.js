'use strict';


const redis = require('redis');
const debug = require('debug');

module.exports = class Redis {
    constructor(host, password, port = 6379, prefix = 'pasger:') {
        if (!this.client) {
            this.client = redis.createClient({
                host, port, password, prefix,
                retry_stragety: function retry(options) {
                    if (options.error.code === 'ECONNREFUSED') {
                        debug(`Redis connection refused: ${options.error.code}`);
                    }
                    if (options.total_retry_time > 1000 * 60 * 60) {
                        return new Error('Retry time exhausted');
                    }
                    return Math.max(options.attempt * 100, 2000);
                },
            });
            this.client.on('error', (err) => {
                debug(`Redis error ${err}`);
                return new Error(err);
            });

            this.client.on('reconnecting', () => {
                debug('Retrying to connect to Redis');
            });
            this.multi = this.client.multi();
        }
    }
    getClient() {
        return this.client;
    }

    getMulti() {
        return this.multi;
    }

    setCache(key, data, time = 1800) {
        this.client.setex(key, time, data);
    }

    get(key) {
        return new Promise((resolve, reject) => {
            this.client.get(key, (err, data) => {
                if (err || !data) {
                    return reject(err);
                }
                resolve(data);
            });
        });
    }

    hget(key, token) {
        return new Promise((resolve, reject) => {
            this.client.hget(key, token, (err, data) => {
                if (err || !data) {
                    return reject(err);
                }
                resolve(data);
            });
        });
    }

    del(key) {
        return new Promise((resolve, reject) => {
            this.client.del(key, (err, rs) => {
                if (err) {
                    return reject(err);
                }
                resolve(rs);
            });
        });
    }

    hset(hash, key, data) {
        return new Promise((resolve, reject) => {
            this.client.hset(hash, key, data, (err, rs) => {
                if (err) {
                    return reject(err);
                }
                resolve(rs);
            });
        });
    }

    keys(prefix) {
        return new Promise((resolve, reject) => {
            this.client.keys(prefix, (err, keys) => {
                if (err) {
                    return reject(err);
                }
                resolve(keys);
            });
        });
    }

    hGetAll(hash) {
        return new Promise((resolve, reject) => {
            this.client.hgetall(hash, (err, object) => {
                if (err) {
                    return reject(err);
                }
                resolve(object);
            });
        });
    }
    SMembers(key) {
        return new Promise((resolve, reject) => {
            this.client.smembers(key, (err, res) => {
                if (err) {
                    return reject(err);
                }
                resolve(res);
            });
        });
    }
    hdel(hash, key) {
        return new Promise((resolve, reject) => {
            this.client.hdel(hash, key, (err, res) => {
                if (err) {
                    return reject(err);
                }
                resolve(res);
            });
        });
    }
};
